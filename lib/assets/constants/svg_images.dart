class SvgImages {
  static const String logo = 'assets/icons/logo.svg';
  static const String call = 'assets/icons/call.svg';
  static const String percent = 'assets/icons/percent.svg';
  static const String search = 'assets/icons/search.svg';
  static const String un_favourite = 'assets/icons/heart.svg';
  static const String favourite = 'assets/icons/heart1.svg';
  static const String home = 'assets/icons/home.svg';
  static const String menu = 'assets/icons/menu.svg';
  static const String shopping = 'assets/icons/shopping.svg';
  static const String user = 'assets/icons/user.svg';
  static const String equa = 'assets/icons/equa.svg';
  static const String medalStar = 'assets/icons/medalStar.svg';
  static const String shoppingCart = 'assets/icons/shoppingCart.svg';
  static const String shield = 'assets/icons/shield.svg';
  static const String moneys = 'assets/icons/moneys.svg';
  static const String question = 'assets/icons/question.svg';
  static const String facebook = 'assets/icons/facebook.svg';
  static const String insta = 'assets/icons/insta.svg';
  static const String telegram = 'assets/icons/telegram.svg';

}
